angular.module("vipApp",['ngRoute','ui.bootstrap'])
.config(function($routeProvider,$httpProvider) {
	$httpProvider.defaults.useXDomain = true;
	$httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
  $routeProvider
   .when('/', {
    templateUrl: 'pages/login.html',
    controller: 'loginController',
    secured:false
  })
    .when('/postLogin', {
    templateUrl: 'pages/postLogin.html',
    controller: 'postLoginController',
      secured:true
  })
    .when('/register',{
      templateUrl:'pages/register.html',
      controller:'registerController',
      secured:true
    })
    .when('/requestPermit',{
      templateUrl:'pages/requestPermit.html',
      controller:'requestPermitController',
      secured:true
    })
    .when('/search',{
      templateUrl:'pages/search.html',
      controller:'searchController',
      secured:true
    })
    .when('/search/:semester',{
      templateUrl:'pages/search.html',
      controller:'searchController',
      secured:true
    })
    .when('/search/:semester/:subject',{
      templateUrl:'pages/search.html',
      controller:'searchController',
      secured:true
    })
    .when('/search/:semester/:subject/:coursenum',{
      templateUrl:'pages/search.html',
      controller:'searchController',
      secured:true
    })
    .when('/user/create/:userType', {
    templateUrl: 'pages/createUser.html',
    controller: 'createUserController',
    secured:false
  })
 });