angular.module('vipApp').controller("postLoginController", ['$scope','$http','$location','$rootScope','searchService',function($scope,$http,$location,$rootScope,searchService){
	var cookies = document.cookie;


//for when login is finished:

	// if(window.localStorage['username']){
	// 	$scope.username = window.localStorage['username'];
	// }else if (cookies.indexOf("username") != -1){
	// 	$scope.username = cookies;
	// }else{
	// 	$location.path("/");
	// }

	//searchService.getSemesters().then(function(result){});
	
	
	if($rootScope.semesters == undefined){
		searchService.getSemesters().then(function(result){
			$rootScope.semesters = JSON.parse(result.data.data);
		});
	}

	
	$scope.setSearch=function($event)
	{
		$location.path("/search")
	}

	$scope.setRegister=function($event)
	{
		$location.path("/register");
	}

	$scope.setViewPermits=function($event)
	{
		$location.path("/viewPermits");
	}

	$scope.setRequestPermit=function($event)
	{
		$location.path("/requestPermit");
	}

	$scope.clearLocalStorage= function(){
		window.localStorage.clear();
	}

//	window.onunload = function(){
//		document.cookie = 'username=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
//	}
}])
