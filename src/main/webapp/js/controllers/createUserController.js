angular.module('vipApp').controller("createUserController", ['$scope','$routeParams','$location','userService',function($scope,$routeParams,$location,userService){
	
	if($routeParams.userType){
		var usertype = $routeParams.userType.charAt(0).toUpperCase() + $routeParams.userType.slice(1);
		if($routeParams.userType === "student"){
			$scope.emailRegex = /^[a-zA-Z]{3}[0-9]{4}$/;
		}else{
			$scope.emailRegex = /^[a-zA-Z]{2,}$/;
		}
		$scope.usertype = usertype;
	}
	$scope.emailExt="@calu.edu";

	$scope.user={};
	$scope.user.firstName = "";
	$scope.user.lastName = "";
	$scope.user.loginId = "";
	$scope.user.email = "";
	$scope.user.password = "";
	$scope.user.reEnterPass="";
	$scope.user.user_type = $routeParams.userType.charAt(0).toLowerCase();

	$scope.submit = function(){
		$scope.user.email = $scope.user.loginId + $scope.emailExt;
		userService.saveUser($scope.user).then(function(result){
			$location.path("/");
		});
	}
	
}])