angular.module('vipApp').controller("searchController", ['$scope','$routeParams','$location','$rootScope','searchService', function($scope,$routeParams,$location,$rootScope,searchService){


/*
	This is the method that starts the screen and loads the screen apporiately.
	First it loads the semester, checks to see if the semester URL param is set.
	If it is, check to make sure it's a value semester.
	If it isn't, reset the url to the base url of '/search'.
	Otherwise, get the courses that could be in that semester.
*/

$scope.selectedCourseSection="";

if($rootScope.semesters == undefined){
	searchService.getSemesters().then(function(result){
		$rootScope.semesters = JSON.parse(result.data.data);
		if($routeParams.semester){
			$scope.selectedSemester = searchService.getSemesterName($routeParams.semester);
			if($scope.selectedSemester == null){
				$location.path("/search");
			}else{
				searchService.getCourseSubjects($scope.selectedSemester).then(function(result){
					$scope.courseSub = JSON.parse(result.data.data);
					if($routeParams.subject && $rootScope.semesters){
						$scope.selectedCourseSub = $routeParams.subject;
					}
				});
			}
		}
	});
}


/*
	Makes sure that the semesters are in place once before moving on.
	This method is only usable if the user is running the application with zero refreshes.
*/
if($routeParams.semester && $rootScope.semesters){
		$scope.selectedSemester = searchService.getSemesterName($routeParams.semester);
		searchService.getCourseSubjects($scope.selectedSemester).then(function(result){
			$scope.courseSub = JSON.parse(result.data.data);
		});
}

if($routeParams.subject && $rootScope.semesters){
	$scope.selectedCourseSub = $routeParams.subject;
}

  //actions that could happen once the page is loaded
  $scope.setSemester = function(item, $event){
	$location.path("/search/"+item.term_value);
	console.log(item);
  }
  $scope.setSubject = function(item, $event){
	$location.path("/search/"+$routeParams.semester+"/"+item.course_subject);
	console.log(item);
  }
  $scope.setCourse = function(item, $event){
	$location.path("/search/"+$routeParams.semester+"/"+$routeParams.subject+"/"+item.courseNumberId);
	console.log(item);
  }

  $scope.submit = function(selectedCourseSection){
  	console.log(selectedCourseSection);
  }
}])
