angular.module('vipApp').controller("loginController", ['$scope','$http','$location','$modal','userService', function($scope,$http,$location,$modal,userService){
	
	if(window.localStorage['username']){
		$location.path("/postLogin");
	}
	
	$scope.user ={};
	$scope.errorMessage = false;
	// $scope.user.remember = false;
	$scope.showModal = function(){
		var modalInstance = $modal.open({
		      templateUrl: 'loginModalContent.html',
		      controller: 'ModalInstanceCtrl',
		    });
		 
		 
	}

	$scope.submit = function(){
		userService.login($scope.user).then(function(result){
			if(result.data.code ==="200"){
				if($scope.user.remember)
					localStorage['username'] = result.data.data;
				else
					document.cookie = "token="+result.data.data;
				$location.path("/postLogin");

			}else if(result.data.code === "403"){
				$scope.errorMessage=true;
				$scope.message = result.data.message;
			}

		});
	};
}]);

angular.module('vipApp').controller('ModalInstanceCtrl', function ($scope, $modalInstance) {

	  $scope.ok = function () {
	    $modalInstance.dismiss('cancel');
	  };
	});
