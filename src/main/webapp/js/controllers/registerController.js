angular.module('vipApp').controller("registerController", ['$scope','$http','$location',function($scope,$http,$location){

	$scope.subRegex = /^[a-zA-Z]{3}$/;
	$scope.numRegex = /^[0-9]{3}$/;

	$scope.form =
	[{
		courseSub: null,
		courseNum: null,
		sectionNum: null
	}];

  	$scope.groups = 
  	[{
      title: "Select Class: ",
      content: "Please fill out the following information...",
      no: 1
    }];

// When a user clicks the + button to add a class
// add a new form for ng-model to store information into
//	as well as a new group for the ng-repeat of the accordion
    $scope.addGroup = function(group, e)
    {
    	if (e)
    	{
      		e.preventDefault();
      		e.stopPropagation();
    	}

    	var newForm = angular.copy($scope.form)
    	$scope.form.splice($scope.form.length, 0, newForm);
    
    	var newGroup = angular.copy(group);
    	newGroup.no = $scope.groups.length + 1;
    	$scope.groups.splice($scope.groups.length, 0, newGroup);
  	};
  
//When a user clicks the - button to remove a class listing
//remove the group at the specified index as well as the form
//	holding any information for that group
	$scope.removeGroup = function(index, e) 
	{
		if (e) 
		{
			e.preventDefault();
			e.stopPropagation();
		}
		$scope.form.splice(index, 1);
		$scope.groups.splice(index, 1);
	};

//test each entered field for regex pass or empty, if it passes, send
// to registerClasses function
    $scope.testUndefined = function()
    {
  		var i = 0;
  		var bad = false;
  		for(i; i < $scope.groups.length; i++)
  		{
  			if($scope.form[i].courseSub == undefined || $scope.form[i].courseSub == null ||
  		   	   $scope.form[i].courseNum == undefined || $scope.form[i].courseNum == null)
  				bad = true;
  		}

  		if(bad)
  			alert("Invalid data entered, please try again");
  		else
  			$scope.registerClasses();
    }

	$scope.registerClasses = function()
    {
	  	var i = 0;
	  	var str = "";
	  	for(i; i < $scope.groups.length; i++)
	  	{
	  		str = str.concat($scope.form[i].courseSub, " ", $scope.form[i].courseNum, "\n");
	  	}

  		alert(str);
  	}

}])