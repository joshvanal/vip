angular.module('vipApp').service('searchService',['$location','$http','$rootScope',function($location,$http,$rootScope){
return{
		getSemesters: function(){
			var request= {
				method:'GET',
				url:"/services/search",
				cache:false
			}
			return $http(request).then(function(result){
				return result;
			});
		},
		getSemesterName:function(semesterId){
			var semester = null;
 			i=0;
 			if($rootScope.semesters){
 				while(semester === null && i != $rootScope.semesters.length){
 					if(($rootScope.semesters[i].term_value === semesterId)){
 						semester = $rootScope.semesters[i];
 					}
 					i++;
 				}
 			}
			return semester;
		}, 
		getCourseSubjects:function(semester){
			var request = {
				method:'GET',
				url:"/services/search?semesterVal="+semester.term_value,
				cache:false
			}

			return $http(request).then(function(result){
				return result;
			});
		}
	};
}]);