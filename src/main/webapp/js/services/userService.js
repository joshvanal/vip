angular.module('vipApp').service('userService',['$location','$http','$rootScope',function($location,$http,$rootScope){
return{
		saveUser: function(user){
			var request= {
				method:'POST',
				url:"/services/user/create",
				data:{
				"user_firstName":user.firstName,
				"user_lastName":user.lastName,
				"user_email":user.email,
				"user_password":user.password,
				"user_login":user.loginId,
				"user_type":user.user_type
				},
				cache:false
			}
			return $http(request).then(function(result){
				return result;
			});
		},

		login: function(user){
			var request={
					method:'POST',
					url:"/services/user/login",
					data:{
						"user_login":user.user_loginId,
						"user_password":user.user_password
					},
					cache:false
			}
			return $http(request).then(function(result){
				return result;
			})
		}
	};
}]);