package edu.calu.vip.db.dao;

import java.util.List;

import edu.calu.vip.db.entity.Permit_Reason;

public interface PermitReasonDao {

	public List<Permit_Reason> findAll();
	public Permit_Reason findById(Long id);
	public boolean updateReasonDesc(Permit_Reason reason);
	public boolean insertReason(Permit_Reason reason);
	public boolean deleteReason(Permit_Reason reason);
}
