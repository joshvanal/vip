package edu.calu.vip.db.dao;

import java.util.List;

import edu.calu.vip.db.entity.Course;
import edu.calu.vip.db.entity.Course_sections;
import edu.calu.vip.db.entity.Term;

public interface SectionsDao {

	public List<Course_sections> findAll();
	public List<Course_sections> findAllBySemester(Term selectedTerm);
	public List<Course> findDistinctCoursesBySemester(Term selectedTerm);
}
