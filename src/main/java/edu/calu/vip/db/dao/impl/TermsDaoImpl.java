package edu.calu.vip.db.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.calu.vip.db.dao.TermsDao;
import edu.calu.vip.db.entity.Term;

@Repository
public class TermsDaoImpl implements TermsDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Term> findAll() {
		// TODO Auto-generated method stub
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		List<Term> terms=this.sessionFactory.getCurrentSession().createCriteria(Term.class).addOrder(Order.asc("start_date")).add(Restrictions.ne("regStatus", "H")).list();
		sessionFactory.getCurrentSession().close();
		return terms; 
	}

	@Override
	public Term findByTermVal(String termVal) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		Term term=(Term) this.sessionFactory.getCurrentSession().createCriteria(Term.class).add(Restrictions.eq("term_value", termVal)).uniqueResult();
		sessionFactory.getCurrentSession().close();
		return term;
	}
	
	@Override
	public Term FindByTermId(int termId){
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		Term term=(Term) this.sessionFactory.getCurrentSession().createCriteria(Term.class).add(Restrictions.eq("term_id", termId)).uniqueResult();
		sessionFactory.getCurrentSession().close();
		return term;
	}

}
