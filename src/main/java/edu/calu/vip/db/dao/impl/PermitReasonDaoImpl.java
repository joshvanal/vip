package edu.calu.vip.db.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.calu.vip.db.dao.PermitReasonDao;
import edu.calu.vip.db.entity.Permit_Reason;

@Repository
public class PermitReasonDaoImpl implements PermitReasonDao {

	@Autowired
	SessionFactory sessionFactory;
	
	public List<Permit_Reason> findAll(){
		List<Permit_Reason> reasons = null;
		Session tx = sessionFactory.openSession();
		try{
			
			tx.beginTransaction();
			reasons = tx.createCriteria(Permit_Reason.class).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		
		return reasons;
	}
	
	public Permit_Reason findById(Long id){
		Permit_Reason reason =null;
		Session tx = sessionFactory.openSession();
		try{
			tx.beginTransaction();
			reason = (Permit_Reason) tx.createCriteria(Permit_Reason.class).add(Restrictions.eq("reason_id", id)).uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return reason;
	}
	
	public boolean updateReasonDesc(Permit_Reason reason){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			tx.update(reason);
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e){
			tx.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return committed;
	}
	
	public boolean insertReason(Permit_Reason reason){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			tx.save(reason);
			tx.getTransaction().commit();
			committed=true;
		}catch(Exception e){
			tx.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return committed;
	}
	
	public boolean deleteReason(Permit_Reason reason){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			tx.delete(reason);
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e){
			tx.getTransaction().rollback();
		}finally{
			tx.close();
		}
		return committed;
	}
}
