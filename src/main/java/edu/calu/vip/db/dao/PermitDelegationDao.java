package edu.calu.vip.db.dao;

import java.util.ArrayList;
import java.util.List;

import edu.calu.vip.db.entity.Permit_Delegation;
import edu.calu.vip.db.entity.User;

public interface PermitDelegationDao {
	public List<Permit_Delegation> findAllbyDelegator(Long userId);
	public List<Permit_Delegation> findAllbyDelegatee(Long userId);
	public boolean insertDelegationList(ArrayList<Permit_Delegation> delegations);
	public boolean deleteDelegationList(ArrayList<Permit_Delegation> delegations);
	public boolean updateDelegation(Permit_Delegation delegation);
}
