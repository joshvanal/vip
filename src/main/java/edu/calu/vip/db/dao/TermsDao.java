package edu.calu.vip.db.dao;

import java.util.List;

import edu.calu.vip.db.entity.Term;

public interface TermsDao {
	public List<Term> findAll();
	public Term findByTermVal(String termVal);
	public Term FindByTermId(int termId);
}
