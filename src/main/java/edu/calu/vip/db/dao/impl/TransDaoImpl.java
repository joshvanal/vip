package edu.calu.vip.db.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.calu.vip.db.dao.TransDao;
import edu.calu.vip.db.entity.Transaction;
import edu.calu.vip.db.entity.User;

@Repository
public class TransDaoImpl implements TransDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Transaction> findAll(){
		Session tx = sessionFactory.openSession();
		List<Transaction> transactions = null;
		try{
			tx.beginTransaction();
			transactions = tx.createCriteria(Transaction.class).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return transactions;
	}
	
	public List<Transaction> findAllByUserId(User user){
		Session tx = sessionFactory.openSession();
		List<Transaction> transactions = null;
		try{
			tx.beginTransaction();
			transactions = tx.createCriteria(Transaction.class).add(Restrictions.eq("trans_studentId", user.getUser_id())).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return transactions;
	}
	
	public Transaction findOneByTransCRN(Transaction trans){
		Session tx = sessionFactory.openSession();
		try{
			tx.beginTransaction();
			trans = (Transaction) tx.createCriteria(Transaction.class).add(Restrictions.eq("trans_crn", trans.getTrans_crn())).uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return trans;
	}

	@Override
	public boolean saveTransactions(List<Transaction> transactions) {
		// TODO Auto-generated method stub
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			for(Transaction trans:transactions){
				tx.save(trans);
			}
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e ){
			e.printStackTrace();
			if(tx != null){
				tx.getTransaction().rollback();
			}
		}finally{
			tx.close();
		}
		return committed;
	}
}
