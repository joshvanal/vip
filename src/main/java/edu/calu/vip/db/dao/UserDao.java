package edu.calu.vip.db.dao;

import java.util.List;

import edu.calu.vip.db.entity.User;

public interface UserDao {
	public List<User> findAll();
	public User findByUserLoginId(User user);
	public boolean saveUser(User user);
	public User findByUserId(Long userId);	
}
