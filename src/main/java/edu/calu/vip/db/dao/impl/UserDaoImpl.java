package edu.calu.vip.db.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.calu.vip.db.dao.UserDao;
import edu.calu.vip.db.entity.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		
		Session tx = sessionFactory.openSession();
		List<User> users = null;
		try{
			tx.beginTransaction();
			users = tx.createCriteria(User.class).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return users;
	}

	@Override
	public User findByUserLoginId(User user) {
		// TODO Auto-generated method stub
		
		Session tx = sessionFactory.openSession();
		User returnUser = null;
		try{
			tx.beginTransaction();
			returnUser = (User) tx.createCriteria(User.class).add(Restrictions.eq("user_login", user.getUser_login().toLowerCase())).add(Restrictions.eq("user_password",user.getUser_password())).uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		
		return returnUser;
	}
	
	@Override
	public User findByUserId(Long userId) {
		// TODO Auto-generated method stub
		
		Session tx = sessionFactory.openSession();
		User returnUser = null;
		try{
			tx.beginTransaction();
			returnUser = (User) tx.createCriteria(User.class).add(Restrictions.eq("user_id",userId)).uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		
		return returnUser;
	}
	
	@Override
	public boolean saveUser(User user) {
		// TODO Auto-generated method stub
		
		System.out.println("Saving user object");
		boolean committed = false;
		Session tx = sessionFactory.openSession();
		try{
		tx.beginTransaction();
		tx.save(user);
		tx.getTransaction().commit();
		committed = true;
		}catch(Exception e){
			e.printStackTrace();
			if(tx != null){
				tx.getTransaction().rollback();
			}
		}finally{
			tx.close();
		}
		return committed;
	}

}
