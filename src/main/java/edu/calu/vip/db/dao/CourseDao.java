package edu.calu.vip.db.dao;

import java.util.ArrayList;
import java.util.List;

import edu.calu.vip.db.entity.Course;
import edu.calu.vip.db.entity.Term;
import edu.calu.vip.db.entity.Transaction;
import edu.calu.vip.db.entity.User;

public interface CourseDao {
	public List<Course> findAll();
	public List<Course> findAllBySemester(Term selectedTerm);
	public List findAllByUserIdFromTrans(User user);
	public List<Course> findAllyByUserId(User user);
	
}
