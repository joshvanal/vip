package edu.calu.vip.db.dao;

import java.util.List;

import edu.calu.vip.db.entity.Transaction;

public interface TransDao {

	boolean saveTransactions(List<Transaction> transactions);

}
