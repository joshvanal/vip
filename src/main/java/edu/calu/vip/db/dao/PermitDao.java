package edu.calu.vip.db.dao;

import java.util.List;

import edu.calu.vip.db.entity.Permit;
import edu.calu.vip.db.entity.User;

public interface PermitDao {
	public List<Permit> findAllbyStudentId(User user);
	public List<Permit> findAllbyProfessorId(User user);
	public boolean updatePermit(Permit permit);
	public boolean insertPermit(Permit permit);
	public Permit findOnebyUserId(User user,Permit permit);
	public Permit findOneByPermit(Permit permit);
}
