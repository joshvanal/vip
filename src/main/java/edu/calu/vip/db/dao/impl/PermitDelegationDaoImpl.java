package edu.calu.vip.db.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.calu.vip.db.dao.PermitDelegationDao;
import edu.calu.vip.db.entity.Permit_Delegation;
import edu.calu.vip.db.entity.User;

@Repository
public class PermitDelegationDaoImpl implements PermitDelegationDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Permit_Delegation> findAllbyDelegator(Long userId){
		Session tx = sessionFactory.openSession();
		List<Permit_Delegation> delegations = null;
		try{
			tx.beginTransaction();
			delegations = tx.createCriteria(Permit_Delegation.class).add(Restrictions.eq("delegation_delegatorId", userId)).list();
		}finally{
			tx.close();
		}
		return delegations;
	}
	
	public List<Permit_Delegation> findAllbyDelegatee(Long userId){
		Session tx = sessionFactory.openSession();
		List<Permit_Delegation> delegations = null;
		try{
			tx.beginTransaction();
			delegations = tx.createCriteria(Permit_Delegation.class).add(Restrictions.eq("delegation_delegateeId", userId)).list();
		}finally{
			tx.close();
		}
		return delegations;
	}
	
	public boolean insertDelegationList(ArrayList<Permit_Delegation> delegations){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			for(Permit_Delegation delegation : delegations){
				tx.save(delegation);
			}
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e){
			tx.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			tx.close();
		}
		
		return committed;
	}
	public boolean deleteDelegationList(ArrayList<Permit_Delegation> delegations){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			for(Permit_Delegation delegation : delegations){
				tx.delete(delegation);
			}
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e){
			tx.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			tx.close();
		}
		
		return committed;
	}
	
	public boolean updateDelegation(Permit_Delegation delegation){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			tx.update(delegation);
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e){
			tx.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			tx.close();
		}
		
		return committed;
	}
}
