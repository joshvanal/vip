package edu.calu.vip.db.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.calu.vip.db.dao.PermitDao;
import edu.calu.vip.db.entity.Permit;
import edu.calu.vip.db.entity.User;

@Repository
public class PermitDaoImpl implements PermitDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Permit> findAllbyStudentId(User user){
		Session tx = sessionFactory.openSession();
		List<Permit> permits = null;
		try{
			tx.beginTransaction();
			permits = tx.createCriteria(Permit.class).add(Restrictions.eq("permit_studentId", user.getUser_id())).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return permits;
		
	}
	
	public List<Permit> findAllbyProfessorId(User user){
		Session tx = sessionFactory.openSession();
		List<Permit> permits = null;
		try{
			tx.beginTransaction();
			permits = tx.createCriteria(Permit.class).add(Restrictions.eq("permit_professorId", user.getUser_id())).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return permits;
		
	}
	
	public boolean updatePermit(Permit permit){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			tx.update(permit);
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e){
			e.printStackTrace();
			tx.getTransaction().rollback();
		}finally{
			tx.close();
		}
		return committed;
	}
	
	public boolean insertPermit(Permit permit){
		Session tx = sessionFactory.openSession();
		boolean committed = false;
		try{
			tx.beginTransaction();
			tx.save(permit);
			tx.getTransaction().commit();
			committed = true;
		}catch(Exception e){
			e.printStackTrace();
			tx.getTransaction().rollback();
		}finally{
			tx.close();
		}
		return committed;
	}
	
	public Permit findOnebyUserId(User user,Permit permit){
		Session tx = sessionFactory.openSession();
		try{
			tx.beginTransaction();
			if(user.getUser_type().equalsIgnoreCase("s")){
				permit = (Permit) tx.createCriteria(Permit.class).add(Restrictions.eq("permit_studentId", user.getUser_id())).add(Restrictions.eq("permit_id", permit.getPermit_id())).uniqueResult();
			}else{
				permit = (Permit) tx.createCriteria(Permit.class).add(Restrictions.eq("permit_professorId", user.getUser_id())).add(Restrictions.eq("permit_id", permit.getPermit_id())).uniqueResult();
			}
		}finally{
			tx.close();
		}
		return permit;
	}

	@Override
	public Permit findOneByPermit(Permit permit) {
		Session tx = sessionFactory.openSession();
		Permit temp = null;
		try{
			tx.beginTransaction();
			permit = (Permit) tx.createCriteria(Permit.class).add(Restrictions.eq("permit_studentId",permit.getPermit_studentId())).add(Restrictions.eq("permit_courseCRN",permit.getPermit_courseCRN()));
		}finally{
			tx.close();
		}
		return null;
	}
}
