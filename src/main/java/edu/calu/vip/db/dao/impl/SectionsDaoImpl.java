package edu.calu.vip.db.dao.impl;

import java.util.List;

import edu.calu.vip.db.dao.SectionsDao;
import edu.calu.vip.db.entity.Course;
import edu.calu.vip.db.entity.Course_sections;
import edu.calu.vip.db.entity.Term;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class SectionsDaoImpl implements SectionsDao  {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Course_sections> findAll() {
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		List<Course_sections>sections=this.sessionFactory.getCurrentSession().createCriteria(Course_sections.class).list();
		sessionFactory.getCurrentSession().close();
		return sections;
	}
	
	public List<Course_sections> findAllBySemester(Term selectedTerm){
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		List<Course_sections>sections=this.sessionFactory.getCurrentSession().createCriteria(Course_sections.class).add(Restrictions.eq("term.term_id", selectedTerm.getTerm_id())).list();
		sessionFactory.getCurrentSession().close();
		
		return sections;
	}
	
	public List<Course> findDistinctCoursesBySemester(Term selectedTerm){
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		List<Course>courses=this.sessionFactory.getCurrentSession().createCriteria(Course_sections.class).setProjection(Projections.distinct(Projections.property("course"))).add(Restrictions.eq("term.term_id", selectedTerm.getTerm_id())).list();
		sessionFactory.getCurrentSession().close();
		return courses;
	}
}
