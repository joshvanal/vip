package edu.calu.vip.db.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.calu.vip.db.dao.CourseDao;
import edu.calu.vip.db.entity.Course;
import edu.calu.vip.db.entity.Term;
import edu.calu.vip.db.entity.Transaction;
import edu.calu.vip.db.entity.User;

@Repository
public class CourseDaoImpl implements CourseDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<Course> findAll() {
		Session tx = sessionFactory.openSession();
		List<Course> returnCourses = null;
		try{
			tx.beginTransaction();
			returnCourses =  tx.createCriteria(Course.class).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return returnCourses;
	}

	@Override
	public List<Course> findAllBySemester(Term selectedTerm) {
		Session tx = sessionFactory.openSession();
		List<Course> returnCourses = null;
		try{
			tx.beginTransaction();
			returnCourses = (List<Course>) tx.createQuery("select distinct c from Course c join fetch c.sections sec where sec.term.term_id = :term_id").setParameter("term_id", selectedTerm.getTerm_id()).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return returnCourses;
	}
	
	public List findAllByUserIdFromTrans(User user){
		Session tx = sessionFactory.openSession();
		List objects = null;
		try{
			tx.beginTransaction();
			Query query =  tx.createQuery("select c,t from Course c join fetch c.sections sec, Transaction t where t.trans_crn = sec.course_CRN and t.trans_studentId = :s_id").setParameter("s_id", user.getUser_id());
			objects = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			tx.close();
		}
		return objects;
	}

	@Override
	public List<Course> findAllyByUserId(User user) {
		// TODO Auto-generated method stub
		return null;
	}
}
