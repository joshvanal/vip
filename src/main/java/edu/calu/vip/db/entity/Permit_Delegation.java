package edu.calu.vip.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="permits",schema="vip")
public class Permit_Delegation {
	@Id
	@GeneratedValue
	@Column(name="permit_id")
	private Long delegation_id;
	
	@Column(name="delegation_delegaterId")
	private Long delegation_delegaterId;
	
	@Column(name="delegation_delegateeId")
	private Long delegation_delegateeId;
	
	@Column(name="delegation_sectionCRN")
	private int delegation_sectionCRN;
	
	@Column(name="delegation_approved",columnDefinition="TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean delegation_approved;

	public Permit_Delegation() {
		super();
	}

	public Permit_Delegation(Long delegation_id, Long delegation_delegaterId,
			Long delegation_delegateeId, int delegation_sectionCRN,
			boolean delegation_approved) {
		super();
		this.delegation_id = delegation_id;
		this.delegation_delegaterId = delegation_delegaterId;
		this.delegation_delegateeId = delegation_delegateeId;
		this.delegation_sectionCRN = delegation_sectionCRN;
		this.delegation_approved = delegation_approved;
	}

	public Long getDelegation_id() {
		return delegation_id;
	}

	public void setDelegation_id(Long delegation_id) {
		this.delegation_id = delegation_id;
	}

	public Long getDelegation_delegaterId() {
		return delegation_delegaterId;
	}

	public void setDelegation_delegaterId(Long delegation_delegaterId) {
		this.delegation_delegaterId = delegation_delegaterId;
	}

	public Long getDelegation_delegateeId() {
		return delegation_delegateeId;
	}

	public void setDelegation_delegateeId(Long delegation_delegateeId) {
		this.delegation_delegateeId = delegation_delegateeId;
	}

	public int getDelegation_sectionCRN() {
		return delegation_sectionCRN;
	}

	public void setDelegation_sectionCRN(int delegation_sectionCRN) {
		this.delegation_sectionCRN = delegation_sectionCRN;
	}

	public boolean isDelegation_approved() {
		return delegation_approved;
	}

	public void setDelegation_approved(boolean delegation_approved) {
		this.delegation_approved = delegation_approved;
	}
	
	
}
