package edu.calu.vip.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transactions",schema="vip")
public class Transaction {

	@Id
	@Column(name="trans_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long trans_id;
	
	@Column(name="trans_crn")
	private int trans_crn;
	
	@Column(name="trans_type")
	private String trans_type;
	
	@Column(name="trans_credits")
	private Double trans_credits;
	
	@Column(name="trans_midterm")
	private String trans_midterm;
	
	@Column(name="trans_final")
	private String trans_final;
	
	@Column(name="trans_studentId")
	private Long trans_studentId;

	public Long getTerm_id() {
		return trans_id;
	}

	public void setTerm_id(Long term_id) {
		this.trans_id = term_id;
	}

	public int getTrans_crn() {
		return trans_crn;
	}

	public void setTrans_crn(int trans_crn) {
		this.trans_crn = trans_crn;
	}

	public String getTrans_type() {
		return trans_type;
	}

	public void setTrans_type(String trans_type) {
		this.trans_type = trans_type;
	}

	public Double getTrans_credits() {
		return trans_credits;
	}

	public void setTrans_credits(Double trans_credits) {
		this.trans_credits = trans_credits;
	}

	public String getTrans_midterm() {
		return trans_midterm;
	}

	public void setTrans_midterm(String trans_midterm) {
		this.trans_midterm = trans_midterm;
	}

	public String getTrans_final() {
		return trans_final;
	}

	public void setTrans_final(String trans_final) {
		this.trans_final = trans_final;
	}

	public Long getTrans_id() {
		return trans_id;
	}

	public void setTrans_id(Long trans_id) {
		this.trans_id = trans_id;
	}

	public Long getTrans_studentId() {
		return trans_studentId;
	}

	public void setTrans_studentId(Long trans_studentId) {
		this.trans_studentId = trans_studentId;
	}
	
	
	
}
