package edu.calu.vip.db.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "terms")
public class Term {

	@Id
	@Column(name="term_id")
	private String term_id;
	
	@Column(name="term_value")
	private String term_value;
	
	@Column(name="term_name")
	private String term_name;
	
	@Column(name="term_startDate")
	private Date start_date;
	
	@Column(name="term_endDate")
	private Date end_date;

	@Column(name="term_regStatus")
	private String regStatus;
	
	@Column(name="INSERT_DATE")
	private Date insert_date;

	public Term(String term_id, String term_value, String term_name,
			Date start_date, Date end_date, String regStatus, Date insert_date) {
		super();
		this.term_id = term_id;
		this.term_value = term_value;
		this.term_name = term_name;
		this.start_date = start_date;
		this.end_date = end_date;
		this.regStatus = regStatus;
		this.insert_date = insert_date;
	}

	public Term() {
		super();
	}

	public String getTerm_id() {
		return term_id;
	}

	public void setTerm_id(String term_id) {
		this.term_id = term_id;
	}

	public String getTerm_value() {
		return term_value;
	}

	public void setTerm_value(String term_value) {
		this.term_value = term_value;
	}

	public String getTerm_name() {
		return term_name;
	}

	public void setTerm_name(String term_name) {
		this.term_name = term_name;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getRegStatus() {
		return regStatus;
	}

	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus;
	}

	public Date getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(Date insert_date) {
		this.insert_date = insert_date;
	}
	
	
	
}
