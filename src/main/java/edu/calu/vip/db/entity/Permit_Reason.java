package edu.calu.vip.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "permit_reasons")
public class Permit_Reason {
	@Id
	@GeneratedValue
	@Column(name="reason_id")
	private Long reason_id;
	
	@Column(name="reason_desc")
	private String reason_desc;

	public Permit_Reason() {
		super();
	}

	public Permit_Reason(Long reason_id, String reason_desc) {
		super();
		this.reason_id = reason_id;
		this.reason_desc = reason_desc;
	}

	public Long getReason_id() {
		return reason_id;
	}

	public void setReason_id(Long reason_id) {
		this.reason_id = reason_id;
	}

	public String getReason_desc() {
		return reason_desc;
	}

	public void setReason_desc(String reason_desc) {
		this.reason_desc = reason_desc;
	}
	
	

}
