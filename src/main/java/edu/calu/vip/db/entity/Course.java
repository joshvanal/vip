package edu.calu.vip.db.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import edu.calu.vip.entity.StudentCourseRecord;

@Entity
@Table(name = "courses",schema="vip")
public class Course {
	@GeneratedValue
	@Id
	@Column(name="course_id")
	private int course_id;
	
	@Column(name="course_subject")
	private String course_subject;
	
	@Column(name="course_num")
	private String course_num;
	
	@Column(name="course_title")
	private String course_title;
	
	@Column(name="Version")
	private int course_version;
	
	@Column(name="hr_course_credits_from")
	private int course_HRCF;
	
	@Column(name="hr_course_credits_to")
	private int course_HRCT;
	
	@Column(name="lec_course_credits_from")
	private int course_LECF;
	
	@Column(name="lec_course_credits_to")
	private int course_LECT;
	
	@Column(name="lab_course_credits_from")
	private int course_LACF;
	
	@Column(name="lab_course_credits_to")
	private int course_LACT;
	
	@Column(name="effectiveDate")
	private int course_effectiveDate;
	
	@Column(name="course_desc")
	private String course_desc;
	
	@Column(name="course_attributes")
	private String attributes;
	
	@Column(name="course_prereq")
	private String prereq;
	
	@Column(name="course_coreq")
	private String coreq;
	
	@OneToMany(mappedBy="course", fetch=FetchType.EAGER)
	 @JsonManagedReference
	private List<Course_sections> sections;
	
	@Transient
	private boolean coReqFlag;
	
	@Transient
	private boolean preReqFlag;

	public Course() {
		super();
	}

	public Course(int course_id, String course_subject, String course_num,
			String course_title, int course_version, int course_HRCF,
			int course_HRCT, int course_LECF, int course_LECT, int course_LACF,
			int course_LACT, int course_effectiveDate, String course_desc,
			String attributes, String prereq, String coreq,
			List<Course_sections> sections) {
		super();
		this.course_id = course_id;
		this.course_subject = course_subject;
		this.course_num = course_num;
		this.course_title = course_title;
		this.course_version = course_version;
		this.course_HRCF = course_HRCF;
		this.course_HRCT = course_HRCT;
		this.course_LECF = course_LECF;
		this.course_LECT = course_LECT;
		this.course_LACF = course_LACF;
		this.course_LACT = course_LACT;
		this.course_effectiveDate = course_effectiveDate;
		this.course_desc = course_desc;
		this.attributes = attributes;
		this.prereq = prereq;
		this.coreq = coreq;
		this.sections = sections;
	}



	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public String getCourse_subject() {
		return course_subject;
	}

	public void setCourse_subject(String course_subject) {
		this.course_subject = course_subject;
	}

	public String getCourse_num() {
		return course_num;
	}

	public void setCourse_num(String course_num) {
		this.course_num = course_num;
	}

	public String getCourse_title() {
		return course_title;
	}

	public void setCourse_title(String course_title) {
		this.course_title = course_title;
	}

	public String getCourse_desc() {
		return course_desc;
	}

	public void setCourse_desc(String course_desc) {
		this.course_desc = course_desc;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public String getPrereq() {
		return prereq;
	}

	public void setPrereq(String prereq) {
		this.prereq = prereq;
	}

	public String getCoreq() {
		return coreq;
	}

	public void setCoreq(String coreq) {
		this.coreq = coreq;
	}

	public List<Course_sections> getSections() {
		return sections;
	}

	public void setSections(List<Course_sections> sections) {
		this.sections = sections;
	}

	public int getCourse_version() {
		return course_version;
	}

	public void setCourse_version(int course_version) {
		this.course_version = course_version;
	}

	public int getCourse_HRCF() {
		return course_HRCF;
	}

	public void setCourse_HRCF(int course_HRCF) {
		this.course_HRCF = course_HRCF;
	}

	public int getCourse_HRCT() {
		return course_HRCT;
	}

	public void setCourse_HRCT(int course_HRCT) {
		this.course_HRCT = course_HRCT;
	}

	public int getCourse_LECF() {
		return course_LECF;
	}

	public void setCourse_LECF(int course_LECF) {
		this.course_LECF = course_LECF;
	}

	public int getCourse_LECT() {
		return course_LECT;
	}

	public void setCourse_LECT(int course_LECT) {
		this.course_LECT = course_LECT;
	}

	public int getCourse_LACF() {
		return course_LACF;
	}

	public void setCourse_LACF(int course_LACF) {
		this.course_LACF = course_LACF;
	}

	public int getCourse_LACT() {
		return course_LACT;
	}

	public void setCourse_LACT(int course_LACT) {
		this.course_LACT = course_LACT;
	}

	public int getCourse_effectiveDate() {
		return course_effectiveDate;
	}

	public void setCourse_effectiveDate(int course_effectiveDate) {
		this.course_effectiveDate = course_effectiveDate;
	}

	public boolean isCoReqFlag() {
		return coReqFlag;
	}

	public void setCoReqFlag(boolean coReqFlag) {
		this.coReqFlag = coReqFlag;
	}

	public boolean isPreReqFlag() {
		return preReqFlag;
	}

	public void setPreReqFlag(boolean preReqFlag) {
		this.preReqFlag = preReqFlag;
	}
		
}
