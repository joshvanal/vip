package edu.calu.vip.db.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


@Entity
@Table(name="permits",schema="vip")
public class Permit {

	@Id
	@GeneratedValue
	@Column(name="permit_id")
	private Long permit_id;
	
	@Column(name="permit_studentId")
	private Long permit_studentId;
	
	@Column(name="permit_professorId")
	private Long permit_professorId;
	
	@Column(name="permit_courseCRN")
	private int permit_courseCRN;
	
	@Column(name="permit_systemReasonId")
	private Long permit_systemReasonId;
	
	@Column(name="permit_studentReason")
	private String permit_studentReason;
	
	@Column(name="permit_approved",columnDefinition="TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean permit_approved;
	
	@Column(name="permit_used",columnDefinition="TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean permit_used;
	
	@Column(name="permit_insertDate")
	private Date permit_insertDate;
	
	@Column(name="permit_updateDate")
	private Date permit_updateDate;

	public Permit() {
		super();
	}

	public Permit(Long permit_id, Long permit_studentId,
			Long permit_professorId, int permit_courseCRN,
			Long permit_systemReasonId, String permit_studentReason,
			boolean permit_approved, boolean permit_used,
			Date permit_insertDate, Date permit_updateDate) {
		super();
		this.permit_id = permit_id;
		this.permit_studentId = permit_studentId;
		this.permit_professorId = permit_professorId;
		this.permit_courseCRN = permit_courseCRN;
		this.permit_systemReasonId = permit_systemReasonId;
		this.permit_studentReason = permit_studentReason;
		this.permit_approved = permit_approved;
		this.permit_used = permit_used;
		this.permit_insertDate = permit_insertDate;
		this.permit_updateDate = permit_updateDate;
	}

	public Long getPermit_id() {
		return permit_id;
	}

	public void setPermit_id(Long permit_id) {
		this.permit_id = permit_id;
	}

	public Long getPermit_studentId() {
		return permit_studentId;
	}

	public void setPermit_studentId(Long permit_studentId) {
		this.permit_studentId = permit_studentId;
	}

	public Long getPermit_professorId() {
		return permit_professorId;
	}

	public void setPermit_professorId(Long permit_professorId) {
		this.permit_professorId = permit_professorId;
	}

	public int getPermit_courseCRN() {
		return permit_courseCRN;
	}

	public void setPermit_courseCRN(int permit_courseCRN) {
		this.permit_courseCRN = permit_courseCRN;
	}

	public Long getPermit_systemReasonId() {
		return permit_systemReasonId;
	}

	public void setPermit_systemReasonId(Long permit_systemReasonId) {
		this.permit_systemReasonId = permit_systemReasonId;
	}

	public String getPermit_studentReason() {
		return permit_studentReason;
	}

	public void setPermit_studentReason(String permit_studentReason) {
		this.permit_studentReason = permit_studentReason;
	}

	public boolean isPermit_approved() {
		return permit_approved;
	}

	public void setPermit_approved(boolean permit_approved) {
		this.permit_approved = permit_approved;
	}

	public boolean isPermit_used() {
		return permit_used;
	}

	public void setPermit_used(boolean permit_used) {
		this.permit_used = permit_used;
	}

	public Date getPermit_insertDate() {
		return permit_insertDate;
	}

	public void setPermit_insertDate(Date permit_insertDate) {
		this.permit_insertDate = permit_insertDate;
	}

	public Date getPermit_updateDate() {
		return permit_updateDate;
	}

	public void setPermit_updateDate(Date permit_updateDate) {
		this.permit_updateDate = permit_updateDate;
	}
		
}
