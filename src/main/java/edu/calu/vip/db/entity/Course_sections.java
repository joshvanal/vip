package edu.calu.vip.db.entity;

import java.sql.Time;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name="course_sections",schema="vip")
public class Course_sections {

	@Id
	@GeneratedValue
	@Column(name="section_id")
	private Integer section_id; 
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="section_termid")
	private Term term;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="section_courseid")
	@JsonBackReference
	private Course course;
	
	@Column(name="CRN")
	private int course_CRN;
	
	@Column(name="section_campus")
	private String campus;
	
	@Column(name="section_days")
	private String days;
	
	@Column(name="section_startTime")
	private Time startTime;
	
	@Column(name="section_EndTime")
	private Time endTime;
	
	@Column(name="section_cap")
	private Integer cap;
	
	@Column(name="section_act")
	private Integer act;
	
	@Column(name="section_remain")
	private Integer remainder;
	
	@Column(name="section_instructor")
	private String instructor;
	
	@Column(name="section_location")
	private String location;
	
	@Transient
	private ArrayList<Course_sections> additionalSections = new ArrayList<Course_sections>();

	public Course_sections() {
		super();
	}

	
	public Course_sections(Integer section_id, Term term, Course course,
			int course_CRN, String campus, String days, Time startTime,
			Time endTime, Integer cap, Integer act, Integer remainder,
			String instructor, String location) {
		super();
		this.section_id = section_id;
		this.term = term;
		this.course = course;
		this.course_CRN = course_CRN;
		this.campus = campus;
		this.days = days;
		this.startTime = startTime;
		this.endTime = endTime;
		this.cap = cap;
		this.act = act;
		this.remainder = remainder;
		this.instructor = instructor;
		this.location = location;
	}


	public int getCourse_CRN() {
		return course_CRN;
	}


	public void setCourse_CRN(int course_CRN) {
		this.course_CRN = course_CRN;
	}


	public Integer getSection_id() {
		return section_id;
	}

	public void setSection_id(Integer section_id) {
		this.section_id = section_id;
	}

	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public Integer getCap() {
		return cap;
	}

	public void setCap(Integer cap) {
		this.cap = cap;
	}

	public Integer getAct() {
		return act;
	}

	public void setAct(Integer act) {
		this.act = act;
	}

	public Integer getRemainder() {
		return remainder;
	}

	public void setRemainder(Integer remainder) {
		this.remainder = remainder;
	}

	public String getInstructor() {
		return instructor;
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
