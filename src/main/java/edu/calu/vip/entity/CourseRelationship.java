package edu.calu.vip.entity;

import java.util.EmptyStackException;
import java.util.Stack;

public class CourseRelationship {
	private int courseId1;
	private int courseId2;
	private String grade1;
	private String grade2;
	private boolean relationType; //true for and, false for or
	private CourseRelationship additionalRelationships1;
	private CourseRelationship additionalRelationships2;
	public int getCourseId1() {
		return courseId1;
	}
	public void setCourseId1(int courseId1) {
		this.courseId1 = courseId1;
	}
	public int getCourseId2() {
		return courseId2;
	}
	public void setCourseId2(int courseId2) {
		this.courseId2 = courseId2;
	}
	public String getGrade1() {
		return grade1;
	}
	public void setGrade1(String grade1) {
		this.grade1 = grade1;
	}
	public String getGrade2() {
		return grade2;
	}
	public void setGrade2(String grade2) {
		this.grade2 = grade2;
	}
	public boolean isAnd() {
		return relationType;
	}
	public void setRelationType(boolean relationType) {
		this.relationType = relationType;
	}
	public CourseRelationship getAdditionalRelationships1() {
		return additionalRelationships1;
	}
	public void setAdditionalRelationships1(
			CourseRelationship additionalRelationships1) {
		this.additionalRelationships1 = additionalRelationships1;
	}
	
	public CourseRelationship getAdditionalRelationships2() {
		return additionalRelationships2;
	}
	public void setAdditionalRelationships2(
			CourseRelationship additionalRelationships2) {
		this.additionalRelationships2 = additionalRelationships2;
	}
	
	public CourseRelationship(int courseId1, int courseId2, String grade1,
			String grade2, boolean relationType,
			CourseRelationship additionalRelationships1,
			CourseRelationship additionalRelationships2) {
		super();
		this.courseId1 = courseId1;
		this.courseId2 = courseId2;
		this.grade1 = grade1;
		this.grade2 = grade2;
		this.relationType = relationType;
		this.additionalRelationships1 = additionalRelationships1;
		this.additionalRelationships2 = additionalRelationships2;
	}
	public CourseRelationship() {
		// TODO Auto-generated constructor stub
	}
	public CourseRelationship generateRelationship(String courseRequirements){
		boolean relParse = false;
		Stack<String> stack1 = new Stack<String>();
		Stack<CourseRelationship> stack2 = new Stack<CourseRelationship>();
		
		for(int i = 0; i < courseRequirements.length();){
			relParse = false; 
			int loc = courseRequirements.substring(i).indexOf("_");
			try{
				Integer.parseInt((String) courseRequirements.subSequence(i,i+1));
			}catch(NumberFormatException e){
				relParse = true;
			}
			if(!relParse){
				stack1.push(courseRequirements.substring(i,i+loc+2));
				i +=loc+2;
			}
			else{
				//found symbol
				String c1 = null;
				String c2 = null;
				CourseRelationship r = new CourseRelationship();
				try{
					c2 = stack1.pop();
					r.setCourseId2(Integer.parseInt(c2.substring(0,1)));
					r.setGrade2((String) c2.subSequence(2, 3));
				}catch(EmptyStackException e){
					r.setAdditionalRelationships2(stack2.pop());
				}
				try{
					c1 = stack1.pop();
					r.setCourseId1(Integer.parseInt(c1.substring(0,1)));
					r.setGrade1((String) c1.subSequence(2, 3));
				}catch(EmptyStackException e){
					r.setAdditionalRelationships1(stack2.pop());
				}
				r.setRelationType(courseRequirements.subSequence(i,i+1).equals(";"));
				stack2.push(r);
				i++;
			}
		}
		if(!stack1.empty()){
			CourseRelationship r = new CourseRelationship();
			String c2 = stack1.pop();
			r.setCourseId2(Integer.parseInt(c2.substring(0,1)));
			r.setGrade2((String) c2.subSequence(2, 3));
			stack2.push(r);
		}
		return stack2.pop(); 
	}
}
