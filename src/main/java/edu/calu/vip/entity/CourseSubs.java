package edu.calu.vip.entity;

import java.util.ArrayList;

import edu.calu.vip.db.entity.Course;

public class CourseSubs {
	
	private String courseSub;
	private ArrayList<Course> courses;
		
	public CourseSubs(String courseSub, ArrayList<Course> courses) {
		super();
		this.courseSub = courseSub;
		this.courses = courses;
	}
	public String getCourseSub() {
		return courseSub;
	}
	public void setCourseSub(String courseSub) {
		this.courseSub = courseSub;
	}
	public ArrayList<Course> getCourses() {
		return courses;
	}
	public void setCourses(ArrayList<Course> courses) {
		this.courses = courses;
	}

}
