package edu.calu.vip.entity;

import java.sql.Time;
import java.util.ArrayList;

public class StudentCourseRecord {
	private int courseId;
	private String courseSub;
	private String courseNum;
	private String courseTitle;
	private String midtermGrade;
	private String finalGrade;
	private ArrayList<TimeRange> timeRange;
	private String termId;

	public StudentCourseRecord() {
		super();
	}

	public StudentCourseRecord(int courseId,String courseSub, String courseNum,
			String courseTitle, String midtermGrade, String finalGrade,
			ArrayList<TimeRange> timeRange, String termId) {
		super();
		this.courseId = courseId;
		this.courseSub = courseSub;
		this.courseNum = courseNum;
		this.courseTitle = courseTitle;
		this.midtermGrade = midtermGrade;
		this.finalGrade = finalGrade;
		this.timeRange = timeRange;
		this.termId = termId;
	}

	public String getCourseSub() {
		return courseSub;
	}

	public void setCourseSub(String courseSub) {
		this.courseSub = courseSub;
	}

	public String getCourseNum() {
		return courseNum;
	}

	public void setCourseNum(String courseNum) {
		this.courseNum = courseNum;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public String getMidtermGrade() {
		return midtermGrade;
	}

	public void setMidtermGrade(String midtermGrade) {
		this.midtermGrade = midtermGrade;
	}

	public String getFinalGrade() {
		return finalGrade;
	}

	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade;
	}

	public String getTermId() {
		return termId;
	}

	public void setTermId(String termId) {
		this.termId = termId;
	}

	public ArrayList<TimeRange> getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(ArrayList<TimeRange> timeRange) {
		this.timeRange = timeRange;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

}
