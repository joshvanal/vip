package edu.calu.vip.entity;

import java.sql.Time;

public class TimeRange {
	private Time startTime;
	private Time endTime;
	private String days;
	public TimeRange() {
		super();
	}

	public TimeRange(Time startTime, Time endTime, String days) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.days = days;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}
	
}
