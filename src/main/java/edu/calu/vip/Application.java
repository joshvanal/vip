package edu.calu.vip;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

//import org.apache.tomcat.jdbc.pool.DataSource;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ComponentScan
@org.springframework.context.annotation.Configuration
@EnableTransactionManagement
public class Application {

	@Bean(name = "dataSource")
    public DataSource getDataSource() {
		DataSource datasource = null;
		try {
			InitialContext ic = new InitialContext();
		    Context initialContext = (Context) ic.lookup("java:comp/env");
		   datasource = (DataSource) initialContext.lookup("jdbc/MySQLDS");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return datasource;
    }
    
	
	private Properties getHibernateProperties() {
    	Properties properties = new Properties();
    	properties.put("hibernate.show_sql", "true");
    	properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
    	properties.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
    	properties.put("hibernate.c3p0.min_size", "5");
    	properties.put("hibernate.c3p0.max_size", "20");
    	properties.put("hibernate.c3p0.timeout", "300");
    	properties.put("hibernate.c3p0.max_statements", "100");
    	properties.put("hibernate.c3p0.idle_test_period", "3000");
    	properties.put("hibernate.current_session_context_class", "thread");
    	return properties;
    }
    
    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) {
    	Configuration cfg = new Configuration();
    	LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
    	sessionBuilder.addProperties(getHibernateProperties());
    	sessionBuilder.scanPackages("edu.calu.vip");
    	return sessionBuilder.buildSessionFactory();
    }
    
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(
			SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(
				sessionFactory);

		return transactionManager;
	}
	
}
