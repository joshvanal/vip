package edu.calu.vip.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Autowired;

import edu.calu.vip.Exception.DuplicatePermitException;
import edu.calu.vip.db.dao.PermitDao;
import edu.calu.vip.db.dao.PermitDelegationDao;
import edu.calu.vip.db.dao.PermitReasonDao;
import edu.calu.vip.db.dao.UserDao;
import edu.calu.vip.db.entity.Permit;
import edu.calu.vip.db.entity.Permit_Delegation;
import edu.calu.vip.db.entity.User;

public class PermitBo {

	@Autowired
	private PermitDao permitDao;
	
	@Autowired
	private PermitReasonDao permitReasonDao;
	
	@Autowired
	private PermitDelegationDao permitDelegationDao;
	
	@Autowired
	private UserDao userDao;
	
	public boolean requestPermit(User user, Permit permit){
		boolean submitted = false;
		boolean sent = false;
		Permit temp = null;
		
		if((temp = permitDao.findOneByPermit(permit))== null){
			submitted = permitDao.insertPermit(permit);
			
			if(submitted){
				ArrayList<User> professors = new ArrayList<User>();
				
				List<Permit_Delegation> delegations = permitDelegationDao.findAllbyDelegator(user.getUser_id());
				if(delegations != null){
					for(Permit_Delegation delegation : delegations){
						professors.add(userDao.findByUserId(delegation.getDelegation_delegateeId()));
					}
				}
				professors.add(userDao.findByUserId(permit.getPermit_professorId()));
				sent = generateRequestedEmail(user, professors, permit);
			}
		}else{
			throw new DuplicatePermitException();
		}
		return sent;
	}
	
	public List<Permit> getAllPermits(User user){
		List<Permit> permits = null;
		
		if(user.getUser_type().equalsIgnoreCase("s")){
			permits = permitDao.findAllbyStudentId(user);
		}else{
			permits = permitDao.findAllbyProfessorId(user);
		}
		
		return permits;
	}
	
	public boolean updatePermit(Permit permit) {
		boolean updated = false;
		
		updated = permitDao.updatePermit(permit);
		
		return updated;
	}
	
	private boolean generateRequestedEmail(User student, ArrayList<User> professors, Permit permit){
		Message message = startMessage();
		
		try{
        message.setFrom(new InternetAddress("noreplyvip@gmail.com"));
        
        InternetAddress[] address = new InternetAddress[professors.size()];
        for(int i = 0; i<professors.size();i++){
        	address[i] = new InternetAddress(professors.get(i).getUser_email());
        }
     
        message.setRecipients(Message.RecipientType.TO,address);
        message.setSubject("Permot Requested");
        
        String text = "<p>"
        		+ "Hello, <br/>"
        		+ "This student, " + student.getUser_firstName() + " " + student.getUser_lastName() + " has requested a permit for this class: <br/>" 
        		+ "Please review his/her request for this course. <br/>"
        		+ "System Reason: <br/>"
        		+ permit.getPermit_systemReasonId() + "<br/>"
        		+ "Student Reason: <br/>"
        		+ permit.getPermit_studentReason() + "<br/>"
        		+ "<a href=\"http://vip-mkj.rhcloud.com/permitRequested/"+student.getUser_login()+"/transcript> "+ student.getUser_firstName()+ " transcript </a> <br/>"
        		+ "<a href=\"http://vip-mkj.rhcloud.com/permit/"+permit.getPermit_id()+"\"> Click here to handle this permit </a>"
        		+ "</p>";
        message.setContent(text, "text/html; charset=utf-8");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sendEmail(message);
	}
	
	
	private Message startMessage(){

        String username = null;
        String password = null;
        
		InitialContext ic;
		try {
			ic = new InitialContext();
			Context initialContext = (Context) ic.lookup("java:comp/env");
		    username = (String) initialContext.lookup("SMTP_FROMEMAIL");
		    password = (String) initialContext.lookup("SMTP_PASSWORD");
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		final String uname = username;
		final String pass = password;
		
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

		
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                  protected PasswordAuthentication getPasswordAuthentication() {
                      return new PasswordAuthentication(uname, pass);
                  }
                });
		return new MimeMessage(session);
	}
	
	private boolean sendEmail(Message message){
        boolean sent = false;
		try {
            Transport.send(message);
            sent = true;

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
		return sent;
	}


}
