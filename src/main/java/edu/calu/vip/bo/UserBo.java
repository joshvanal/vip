package edu.calu.vip.bo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.calu.vip.db.entity.Course;
import edu.calu.vip.db.entity.Course_sections;
import edu.calu.vip.db.entity.Transaction;
import edu.calu.vip.db.entity.User;
import edu.calu.vip.entity.CourseRelationship;
import edu.calu.vip.entity.StudentCourseRecord;
import edu.calu.vip.entity.TimeRange;

public class UserBo {

	
	public static String EncryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.update(password.getBytes("UTF-8"));
		byte[] bytes = digest.digest();
		StringBuilder sb = new StringBuilder();
	    for(int j=0; j< bytes.length ;j++)
	    {
	        sb.append(Integer.toString((bytes[j] & 0xff) + 0x100, 16).substring(1));
	    }
		
		
		return sb.toString();
	}
	
	public static String base64UserObj(User user) throws JsonProcessingException{
		ObjectMapper map = new ObjectMapper();
		byte[] encodedBytes;
		String userObj = map.writeValueAsString(user);			
		encodedBytes = Base64.encodeBase64(userObj.getBytes());
		return new String(encodedBytes);
	}
	
	public static User decodeUserObj(String encoded) throws IOException{
		ObjectMapper map = new ObjectMapper();
		User user = null;
		byte[] decodedBytes = Base64.decodeBase64(encoded);
		user = map.readValue(decodedBytes, User.class);			
		
		return user;
	}

	public static List<StudentCourseRecord> getStudentRecords(List<Object[]> objects) {
		// TODO Auto-generated method stub
		ArrayList<StudentCourseRecord> recs = new ArrayList<StudentCourseRecord>();
		for(Object[] obj:objects){
			Course course = (Course) obj[0];
			Transaction trans = (Transaction) obj[1];
			ArrayList<TimeRange> ranges = new ArrayList<TimeRange>();
			for(Course_sections sections : course.getSections()){
				ranges.add(new TimeRange(sections.getStartTime(),sections.getEndTime(),sections.getDays()));
			}
			recs.add(new StudentCourseRecord(course.getCourse_id(),course.getCourse_subject(),course.getCourse_num(),course.getCourse_title(),trans.getTrans_midterm(),trans.getTrans_final(),ranges,course.getSections().get(0).getTerm().getTerm_id()));
			
		}
		return recs;
	}

	public static List<Transaction> testRequestedCourses(
			ArrayList<Course> requestedCourses, List<StudentCourseRecord> records,User user) {
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		requestedCourses = timeConflictTest(requestedCourses, (ArrayList<StudentCourseRecord>) records);
		HashMap<Integer,String> studentCourseToGrade = new HashMap<Integer,String>();
		
		for(StudentCourseRecord rec: records){
			studentCourseToGrade.put(rec.getCourseId(), rec.getFinalGrade());
		}
		
		
		for(Course course:requestedCourses){
			String prereq = course.getPrereq();
			String coreq = course.getCoreq();
			CourseRelationship coRel = new CourseRelationship();
			CourseRelationship preRel = new CourseRelationship();
			if(coreq != null){
			coRel = coRel.generateRelationship(coreq);
			course.setCoReqFlag(CompareRequirementsToRecords(coRel,studentCourseToGrade));
			}
			if(prereq != null){
			preRel = preRel.generateRelationship(prereq);
			course.setPreReqFlag(CompareRequirementsToRecords(preRel, studentCourseToGrade));
			}
		}
		
		for(int i = 0; i < requestedCourses.size(); i++){
			if(!requestedCourses.get(i).isCoReqFlag() || !requestedCourses.get(i).isPreReqFlag()){
				requestedCourses.remove(i);
				i--;
			}
		}
		studentCourseToGrade.clear();
		records.clear();
		for(Course course: requestedCourses){
			Transaction trans = new Transaction();
			trans.setTerm_id(Long.parseLong(course.getSections().get(0).getTerm().getTerm_id()));
			trans.setTrans_credits((double) course.getCourse_LECF());
			trans.setTrans_crn(course.getSections().get(0).getCourse_CRN());
			trans.setTrans_studentId(user.getUser_id());
			trans.setTrans_type("R");
			transactions.add(trans);
		}
		
		return transactions;
	}
	
	private static boolean CompareRequirementsToRecords(CourseRelationship rel, HashMap<Integer,String> studentCourses){
		boolean approved = true;
		Integer c2 = rel.getCourseId2();
		Integer c1 = rel.getCourseId1();
		if(rel.isAnd()){
			//and
			if(rel.getAdditionalRelationships2() != null){
				if(rel.getAdditionalRelationships1() != null){
					if(!CompareRequirementsToRecords(rel.getAdditionalRelationships2(), studentCourses) && !CompareRequirementsToRecords(rel.getAdditionalRelationships1(), studentCourses)){
						approved = false;
					}
				}else{
					if(c2  != null){
						if(!(studentCourses.get(c2).toUpperCase().compareTo(rel.getGrade2().toUpperCase()) >= 0 || studentCourses.get(c2).equalsIgnoreCase("P"))&& !CompareRequirementsToRecords(rel.getAdditionalRelationships2(), studentCourses)){
							approved=false;
						}
					}
				}
			}else{
				if(c2 != null){
					if(c1 != null){
						if(!(studentCourses.get(c2).toUpperCase().compareTo(rel.getGrade2().toUpperCase()) >= 0 || studentCourses.get(c2).equalsIgnoreCase("P")) && !(studentCourses.get(c1).toUpperCase().compareTo(rel.getGrade1().toUpperCase()) >= 0 || studentCourses.get(c1).equalsIgnoreCase("P"))){
							approved = false;
						}
					}else{
						if(!(studentCourses.get(c2).toUpperCase().compareTo(rel.getGrade2().toUpperCase()) >= 0 || studentCourses.get(c2).equalsIgnoreCase("P"))){
							approved = false;
						}
					}
				}
			}
			
		}else{
			//or
			approved = false;
			if(rel.getAdditionalRelationships2() != null){
				if(rel.getAdditionalRelationships1() != null){
					if(CompareRequirementsToRecords(rel.getAdditionalRelationships2(), studentCourses) || CompareRequirementsToRecords(rel.getAdditionalRelationships1(), studentCourses)){
						approved = true;
					}
				}else{
					if(c2  != null){
						if((studentCourses.get(c2).toUpperCase().compareTo(rel.getGrade2().toUpperCase()) >= 0 || studentCourses.get(c2).equalsIgnoreCase("P"))|| CompareRequirementsToRecords(rel.getAdditionalRelationships2(), studentCourses)){
							approved=true;
						}
					}
				}
			}else{
				if(c2 != null){
					if(c1 != null){
						if((studentCourses.get(c2).toUpperCase().compareTo(rel.getGrade2().toUpperCase()) >= 0 || studentCourses.get(c2).equalsIgnoreCase("P")) || (studentCourses.get(c1).toUpperCase().compareTo(rel.getGrade1().toUpperCase()) >= 0 || studentCourses.get(c1).equalsIgnoreCase("P"))){
							approved = true;
						}
					}else{
						if((studentCourses.get(c2).toUpperCase().compareTo(rel.getGrade2().toUpperCase()) >= 0 || studentCourses.get(c2).equalsIgnoreCase("P"))){
							approved = true;
						}
					}
				}
			}
		}
		return approved;
	}
	
	public static ArrayList<Course> timeConflictTest(ArrayList<Course> requestedCourses, ArrayList<StudentCourseRecord> records){
		int i,j;
		boolean conflict = false;
		for(i = 0; i < requestedCourses.size();i++){
			ArrayList<Course_sections> sections = (ArrayList<Course_sections>) requestedCourses.get(i).getSections();
			for(j=i+1;j<requestedCourses.size();j++){
				ArrayList<Course_sections> sections2 = (ArrayList<Course_sections>) requestedCourses.get(j).getSections();
				for(Course_sections sec: sections){
					for(Course_sections sec2: sections2){
						if(sec.getStartTime().compareTo(sec2.getStartTime())>= 0 && sec.getEndTime().compareTo(sec2.getEndTime()) <=0 ){
							conflict = true;
							break;
						}
					}
					if(conflict){
						break;
					}
				}
				if(conflict){
					requestedCourses.remove(j);
					break;
				}
				
			}
			if(conflict){
				requestedCourses.remove(i);
				conflict = false;
				i--;
			}
		}
		
		ArrayList<StudentCourseRecord> tempList = new ArrayList<StudentCourseRecord>();
		for(StudentCourseRecord rec : records){
			if(rec.getTermId() == requestedCourses.get(0).getSections().get(0).getTerm().getTerm_id()){
				tempList.add(rec);
			}
		}
		for(i = 0;i<records.size();i++){
			ArrayList<TimeRange> range = records.get(i).getTimeRange();
			for(j=0;j<requestedCourses.size();j++){
				ArrayList<Course_sections> sections = (ArrayList<Course_sections>) requestedCourses.get(j).getSections();
				
				for(TimeRange r: range){
					for(Course_sections sec: sections){
						if(sec.getStartTime().compareTo(r.getStartTime()) >=0 && sec.getEndTime().compareTo(r.getEndTime()) <= 0){
							conflict = true;
							break;
						}
					}
					conflict = true;
					break;
				}
				requestedCourses.remove(j);
				j--;
			}
		}
		return requestedCourses;
		
	}
}
