package edu.calu.vip.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.calu.vip.db.dao.CourseDao;
import edu.calu.vip.db.dao.SectionsDao;
import edu.calu.vip.db.dao.TermsDao;
import edu.calu.vip.db.entity.Course;
import edu.calu.vip.db.entity.Course_sections;
import edu.calu.vip.db.entity.Term;
import edu.calu.vip.entity.ResponseMessage;

@RestController
public class Search {
	
	@Autowired
	private SectionsDao sectionDao;
	
	@Autowired
	private TermsDao termsDao;
	
	@Autowired
	private CourseDao courseDao;
	
//	@Resource
//	@Qualifier("courseHashMap")
//	private Map<Long, Course> courseHashMap;
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ResponseMessage searchClasses(
			HttpServletRequest request,
			@RequestParam(value = "semesterVal", required = false) String semesterVal,
			@RequestParam(value = "courseSub", required = false) String courseSub,
			@RequestParam(value = "courseNum", required = false) String courseNum) {
		ObjectMapper map = new ObjectMapper();
		map.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm a z"));
		ResponseMessage res = new ResponseMessage();
		if (semesterVal == null) {
			System.out.println("Entered searching semesters method");
			ArrayList<Term> terms = new ArrayList<Term>();
			terms = (ArrayList<Term>) termsDao.findAll();
			res.setCode("200");
			try {
				res.setData(map.writeValueAsString(terms));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			res.setMessage("No semester submitted");
		} else {
			Term selectedTerm = termsDao.findByTermVal(semesterVal);
			if(selectedTerm != null){
				if (courseSub == null) {
					System.out.println("Entered searching courses method");
					ArrayList<Course> courses = (ArrayList<Course>) courseDao.findAllBySemester(selectedTerm);
					
					res.setCode("200");
					try {
						res.setData(map.writeValueAsString(courses));
					} catch (JsonProcessingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					if (courseNum == null) {
						// search for course numbers based on term_value and
						// course_subject
					} else {

					}
				}
			}else{
				res.setCode("404");
				res.setMessage("Could not find semester");
				res.setData(null);
			}
		}
		
		return res;
	}

}
