package edu.calu.vip.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.calu.vip.bo.PermitBo;
import edu.calu.vip.bo.UserBo;
import edu.calu.vip.db.dao.CourseDao;
import edu.calu.vip.db.dao.TransDao;
import edu.calu.vip.db.dao.UserDao;
import edu.calu.vip.db.dao.impl.TransDaoImpl;
import edu.calu.vip.db.entity.Course;
import edu.calu.vip.db.entity.Transaction;
import edu.calu.vip.db.entity.User;
import edu.calu.vip.entity.ResponseMessage;
import edu.calu.vip.entity.StudentCourseRecord;

@RestController
@RequestMapping(value="/user")
public class UserService {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private CourseDao courseDao;
	
	@Autowired
	private TransDao transDao;
	
	private static int counter = 0;
@RequestMapping(value="/login",method=RequestMethod.POST)
public ResponseMessage login(@RequestBody User user,HttpServletRequest res){
	ResponseMessage msg = new ResponseMessage();
	try {
		user.setUser_password(UserBo.EncryptPassword(user.getUser_password()));
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	user = userDao.findByUserLoginId(user);
	if(user != null){
		try{
			msg.setData(UserBo.base64UserObj(user));
		}catch(Exception e){
			e.printStackTrace();
		}
		msg.setCode("200");
		msg.setMessage("Status:Ok");
	}else{
		msg.setCode("403");
		msg.setData(null);
		msg.setMessage("Invalid username/password");
	}
	counter++;
	System.out.println("Current count: " + counter);
	return msg;
}

@RequestMapping(value="/courses", method=RequestMethod.GET)
public ResponseMessage getCourses(HttpServletRequest req){
	ResponseMessage msg = new ResponseMessage();
	ObjectMapper mapper = new ObjectMapper();
	Cookie[] c = req.getCookies();
	String userCookie = c[0].getValue();
	User user = null;
	try{
		user = UserBo.decodeUserObj(userCookie);
		switch(user.getUser_type().getBytes()[0]){
		case 's':
			List objects = courseDao.findAllByUserIdFromTrans(user);
			List<StudentCourseRecord> records = UserBo.getStudentRecords(objects);
			msg.setData(mapper.writeValueAsString(records));
			break;
		case 'p':
			break;
		default:
			break;
		}
		msg.setCode("200");
		msg.setMessage(null);
	}catch(Exception e){
		e.printStackTrace();
		msg.setCode("405");
		msg.setMessage("System Error. Please contact your system adminstrator.");
	}
	return msg;
}

@RequestMapping(value="/create", method=RequestMethod.POST)
public ResponseMessage createUser(@RequestBody User user, HttpServletRequest res){
	ResponseMessage msg = new ResponseMessage();
	ObjectMapper map = new ObjectMapper();
	boolean committed = false;
	try {
		user.setUser_password(UserBo.EncryptPassword(user.getUser_password()));
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	user.setUser_login(user.getUser_login().toLowerCase());
	committed = userDao.saveUser(user);
	user.setUser_password(null);
	if(committed){
	msg.setCode("200");
	msg.setMessage("Your username is " + user.getUser_login());
	}
	try {
		msg.setData(map.writeValueAsString(user));
	} catch (JsonProcessingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return msg;
}

@RequestMapping(value="/register", method=RequestMethod.POST)
public ResponseMessage register(@RequestBody ArrayList<Course> courses, HttpServletRequest req){
	ResponseMessage msg = new ResponseMessage();
	Cookie[] c = req.getCookies();
	String userCookie = c[0].getValue();
	User user = null;
	boolean committed=false;
	try{
		user = UserBo.decodeUserObj(userCookie);
		List objects = courseDao.findAllByUserIdFromTrans(user);
		List<StudentCourseRecord> records = UserBo.getStudentRecords(objects);
		List<Transaction> transactions = UserBo.testRequestedCourses(courses,records,user);
		committed = transDao.saveTransactions(transactions);
		if(committed){
			msg.setCode("200");
			if(courses.size() > transactions.size()){
				if(courses.size() > 1){
					msg.setCode("505");
					msg.setMessage("Some of the requested courses had some issue. Please fill out a permit for the missing courses.");
				}
				else{
					msg.setCode("503");
					msg.setMessage("The requested course had some issue registering. Please fill out a permit for the course.");
				}
					
			}else{
				msg.setCode("200");
				msg.setMessage("Your classes has been saved");
			}
			msg.setData(null);
		}else{
			msg.setCode("403");
			msg.setMessage("Please login again.");
			msg.setData(null);
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	
	return msg;
}

}
