package edu.calu.vip.services;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.calu.vip.entity.LoginCreds;
import edu.calu.vip.entity.ResponseMessage;

@RestController
public class Login {
	private static int counter = 0;
    @RequestMapping(value = "/login", method = RequestMethod.POST,consumes="application/json", produces="application/json")
    public ResponseMessage login(@RequestBody LoginCreds login) {
    	ResponseMessage message = new ResponseMessage();
    	if(login.getUsername().equalsIgnoreCase(login.getPassword())){
    		message.setData(login.getUsername()+":"+login.getPassword());
    		message.setCode("200");
    		message.setMessage("Status:Ok");
    		counter++;
    		System.out.println("Current count: " + counter);
    	}else{
    		message.setCode("403");
    		message.setData(login.getUsername()+":"+login.getPassword());
    		message.setMessage("Invalid username/password");
    	}
    	
    	return message;
    }
  
}
