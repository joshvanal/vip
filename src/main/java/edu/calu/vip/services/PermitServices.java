package edu.calu.vip.services;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.calu.vip.Exception.DuplicatePermitException;
import edu.calu.vip.bo.PermitBo;
import edu.calu.vip.bo.UserBo;
import edu.calu.vip.db.entity.Permit;
import edu.calu.vip.db.entity.User;
import edu.calu.vip.entity.ResponseMessage;

@RequestMapping("/permit")
public class PermitServices {
	PermitBo permitBo = new PermitBo();
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseMessage sendPermit(@RequestBody Permit permit, HttpServletRequest req){
		ResponseMessage message = new ResponseMessage();
		boolean sent = false;
		//validate user
		User user = null;
		Cookie[] c = req.getCookies();
		String userCookie = c[0].getValue();
		
		try{
			user = UserBo.decodeUserObj(userCookie);
			sent = permitBo.requestPermit(user, permit);
			if(sent){
				message.setCode("200");
				message.setMessage("Your request has been submitted.");
				message.setData(null);
			}else{
				message.setCode("500");
				message.setMessage("Your request failed to send. Please try again.");
				message.setData(null);
			}
		}catch(DuplicatePermitException e){
			message.setCode("403");
			message.setMessage("You've already submitted this request. Please send a different permit.");
			message.setData(null);
		}catch(IOException e){
			message.setCode("500");
			message.setMessage("You are not a registered student.");
			message.setData(null);
		}
		return message;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseMessage getPermits(HttpServletRequest req){
		ResponseMessage message = new ResponseMessage();
		ObjectMapper map = new ObjectMapper();
		List<Permit> permits = null;
		User user = null;
		Cookie[] c = req.getCookies();
		String userCookie = c[0].getValue();
		try{
			user = UserBo.decodeUserObj(userCookie);
			permits = permitBo.getAllPermits(user);
			if(permits != null){
				message.setData(map.writeValueAsString(permits));
			}else{
				message.setData(null);
			}
			message.setCode("200");
			message.setMessage(null);
		}catch(JsonProcessingException e){
			message.setCode("500");
			message.setMessage("System error. Please contact UTech.");
		}catch(IOException e){
			message.setCode("403");
			message.setMessage("No user cookie. Please sign in again.");
		}
		
		return message;
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseMessage updatePermit(@RequestBody Permit permit, HttpServletRequest req){
		ResponseMessage message = new ResponseMessage();
		boolean updated = false;
		User user = null;
		Cookie[] c = req.getCookies();
		String userCookie = c[0].getValue();
		try{
			user = UserBo.decodeUserObj(userCookie);
			updated = permitBo.updatePermit(permit);
			if(updated){
				message.setCode("200");
				message.setMessage("This request has been updated.");
			}else{
				message.setCode("500");
				message.setMessage("This request couldn't be updated. Please try again");
			}
		}catch(IOException e){
			message.setCode("403");
			message.setMessage("No user cookie. Please sign in again.");
		}
		return message;
	}
}
