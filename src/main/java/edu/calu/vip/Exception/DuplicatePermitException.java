package edu.calu.vip.Exception;

public class DuplicatePermitException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6673087505248182505L;

	public DuplicatePermitException(){
		super();
	}
	
	public DuplicatePermitException(String message){
		super(message);
	}
	
	public DuplicatePermitException(String message, Throwable cause){
		super(message, cause);
	}
	
	public DuplicatePermitException(Throwable cause){
		super(cause);
	}
}
